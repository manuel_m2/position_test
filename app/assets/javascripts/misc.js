$(document).ready(function () {

    function showTip(element, message) {
      $(element).qtip({
        content: {
          text: message
        },
        position: {
          my: "Bottom Center",
          at: "Top Center"
        },
        show: {
          ready: true
        }
      });
    }

    function validateUrl(){
      var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
      if ($('#web_url').val() === ''){
        showTip("#web_url",'URL');
        $('#web_url').focus();
        return false;
      }else if (!regex.test($('#web_url').val())){
        showTip("#web_url",'URL invalida');
        $('#web_url').focus();
        return false;
      }
      return true
    }

    $("#search-btn").click(function(event) {
      if (validateUrl() === true){
        $("#search-form").submit();
      }
      event.preventDefault();
    });

});