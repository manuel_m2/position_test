class ScrapsController < ApplicationController

    layout 'admin'

    def index
        images = 0
        css = 0
        css_inside = 0
        @scraps = []

        if params[:web_url]
            page = HTTParty.get(params[:web_url])
            parse_page = Nokogiri::HTML(page)

            hash = { 
                images: parse_page.search('img').count, 
                css: parse_page.search('style').count,
                css_inside: parse_page.css('body', 'style').count
            }

            @scraps.push(hash)
        end
    end

end
